import { Db, MongoClient } from 'mongodb';

const url = 'mongodb://localhost:27017';
const dbName = 'gastro-smart-db';

const client = new MongoClient(url, { useUnifiedTopology: true });

export let db;

export const dbConnect = async(name = dbName) => {
  const connection = await client.connect();
  db = connection.db();
  return client;
}
