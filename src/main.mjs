import fs from 'node:fs';
import { H2Server } from './server.mjs';
import { dbConnect } from './mongodb.mjs';

dbConnect().then(() => console.info('Connected to MongoDB'));

const ssl = {
  key: fs.readFileSync('./assets/certs/localhost-privkey.pem'),
  cert: fs.readFileSync('./assets/certs/localhost-cert.pem')
}
const options = {
  ssl,
  useWebSockets: true,
  allowHTTP1: true
}

const server = new H2Server(options);

server.listen('8443', 'localhost');
